package com.akash.dynamicimages

import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkRequest
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.akash.dynamicimages.databinding.MainActivityBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.android.material.snackbar.Snackbar
import java.util.*
import kotlin.concurrent.thread

class MainActivity : AppCompatActivity() {

    lateinit var mainActivityBinding: MainActivityBinding
    private lateinit var mConnectivityManager: ConnectivityManager
    private var isNetworkConnected = false
    private var IMAGE_URL = "https://picsum.photos/1280/720"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainActivityBinding = MainActivityBinding.inflate(layoutInflater)
        setContentView(mainActivityBinding.root)

        mConnectivityManager = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager

        initNetworkCallback()

        mainActivityBinding.buttonUpdate.setOnClickListener {
            if (isNetworkConnected) {
                loadFreshImage()
            } else {
                Snackbar.make(
                    mainActivityBinding.mainLayout,
                    getString(R.string.str_message_not_connected),
                    Snackbar.LENGTH_LONG
                ).setAction(getString(R.string.label_close)) { }
                    .show()
            }
        }

        loadImage(IMAGE_URL, true)

    }

    private fun loadImage(url: String, isFirstTime: Boolean = false) {
        with(mainActivityBinding.imageView) image@{
            Glide.with(this.context).load(url).onlyRetrieveFromCache(isFirstTime)
                .skipMemoryCache(true)
                .error(R.drawable.placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(this@image)
        }
    }

    private fun loadFreshImage(firstTime: Boolean = false) {
        thread {
            // Glide use image path as key to store cached image. Because We are using static image path
            // It is needed to clear cache before loading new image, otherwise glide will always load old image stored in cache
            Glide.get(this)
                .clearDiskCache()
            //This method needs to executed in main thread, otherwise the app will crash
            runOnUiThread {
                Glide.get(this)
                    .clearMemory()
                loadImage(IMAGE_URL, firstTime)

            }
        }
    }

    private fun initNetworkCallback() {
        val builder: NetworkRequest.Builder = NetworkRequest.Builder()
        mConnectivityManager.registerNetworkCallback(builder.build(),
            object : ConnectivityManager.NetworkCallback() {
                override fun onAvailable(network: Network) {
                    super.onAvailable(network)
                    isNetworkConnected = true
                }

                override fun onLost(network: Network) {
                    super.onLost(network)
                    isNetworkConnected = false
                    Snackbar.make(
                        mainActivityBinding.mainLayout,
                        getString(R.string.str_message_not_connected),
                        Snackbar.LENGTH_LONG
                    ).setAction(getString(R.string.label_close)) { }
                        .show()
                }

            })

    }

    override fun onDestroy() {
        super.onDestroy()
        try {
            mConnectivityManager.unregisterNetworkCallback(ConnectivityManager.NetworkCallback())
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

}